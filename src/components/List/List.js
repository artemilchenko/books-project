import styles from './List.module.css'
import {Book} from "../Book/Book";
import {Navigate} from "../Navigate/Navigate";
import {useDispatch, useSelector} from "react-redux";
import {useGetBooks} from "../../api/models/book/hook/getBooksHook";
import {useState} from "react";

export function List() {
    const dispatch = useDispatch()
    const {loading, data, error} = useGetBooks()
    const [skip, setSkip] = useState(12)

    return (
        <div className={styles.list}>
            <Navigate setSkip={setSkip}/>
            <div className={styles.items}>
                {data?.sort((a, b) => a.collections.popularity - b.collections.popularity).slice(0, skip).map(book =>
                    <Book
                        key={book.id}
                        bookOne={book}/>
                )}
            </div>
            <button
                onClick={() => {
                    setSkip(skip + 12)
                }}
                className={styles.items__button}>Показать еще
            </button>
        </div>
    )
}
