import {useEffect, useState} from "react";
import {AxiosError} from "axios";
import books from '../../../../games_test.json';
import {useSelector} from "react-redux";

export function useGetBooks() {
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(null)
    const [error, setError] = useState(null)
    let providerState = useSelector(state => state.app.provider)
    let currencyState = useSelector(state => state.app.currency)

    async function getBooks() {
        try {
            setError(null)
            setLoading(true)
            const response = await new Promise((resolve, reject) => {
                setTimeout(() => {
                    let arr = []
                    for (let obj in books) {
                        const book = {id: obj, ...books[obj]}
                        arr.push(book)
                    }
                    resolve(arr)
                }, 500)
            }).then((data) => {
                if (providerState) data = data.filter(book => book.provider === providerState)
                if (currencyState) data = data.filter(book => Object.keys(book.real).includes(currencyState))
                return data
            })
            setData(response)
            setLoading(false)
        } catch (err) {
            if (err instanceof AxiosError) {
                if (err.response) {
                    setError(err.message)
                    setLoading(false)
                }
            }
        }
    }

    useEffect(() => {
        getBooks()
    }, [currencyState, providerState])

    return {loading, data, error, getBooks}
}
