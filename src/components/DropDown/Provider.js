import * as React from 'react';
import Box from '@mui/material/Box';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import {setProviderSlice} from "../../store/app.slice";
import {useDispatch} from "react-redux";

export function BasicSelectProvider({setSkip}) {
    const [provider, setProvider] = React.useState('');
    const dispatch = useDispatch()

    const handleChange = (event) => {
        dispatch(setProviderSlice(event.target.value))
        setSkip(12)
        setProvider(event.target.value);
    };

    return (
        <Box sx={{ minWidth: 120 }}>
            <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Provider</InputLabel>
                <Select
                    labelId="demo-simple-select-label"
                    id="demo-simple-select"
                    value={provider}
                    label="Provider"
                    onChange={handleChange}
                >
                    <MenuItem value={''}>Reset</MenuItem>
                    <MenuItem value={'bgaming'}>Bgaming</MenuItem>
                    <MenuItem value={'booming'}>Booming</MenuItem>
                    <MenuItem value={'evolution'}>Evolution</MenuItem>
                    <MenuItem value={'pragmaticplay'}>Pragmaticplay</MenuItem>
                    <MenuItem value={'amatic'}>Amatic</MenuItem>
                    <MenuItem value={'belatra'}>Belatra</MenuItem>
                    <MenuItem value={'habanero'}>Habanero</MenuItem>
                    <MenuItem value={'gameart'}>Gameart</MenuItem>
                    <MenuItem value={'spinomenal'}>Spinomenal</MenuItem>
                    <MenuItem value={'nucleus'}>Nucleus</MenuItem>
                    <MenuItem value={'mancala'}>Mancala</MenuItem>
                    <MenuItem value={'platipus'}>Platipus</MenuItem>
                    <MenuItem value={'truelab'}>Truelab</MenuItem>
                    <MenuItem value={'tomhorn'}>Tomhorn</MenuItem>
                </Select>
            </FormControl>
        </Box>
    );
}
