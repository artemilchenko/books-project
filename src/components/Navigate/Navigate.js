import styles from "./Navigate.module.css";
import {BasicSelectCurrency} from "../DropDown/Currency";
import {BasicSelectProvider} from "../DropDown/Provider";


export function Navigate({setSkip}) {
    return (
        <div className={styles.navigate}>
            <div className={styles.drop}>
                <BasicSelectCurrency setSkip={setSkip}/>
            </div>
            <div className={styles.drop}>
                <BasicSelectProvider setSkip={setSkip}/>
            </div>
        </div>
    )
}
